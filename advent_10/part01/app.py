def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

if __name__ == '__main__':
    raw_input = [int(x) for x in read_input()]
    sorted_jolts = [0] + sorted(raw_input) + [max(raw_input) + 3]
    pairwise_inputs = list(zip(sorted_jolts, sorted_jolts[1:]))
    differences = [high - low for low, high in pairwise_inputs]
    
    ones = len([x for x in differences if x == 1])
    threes = len([x for x in differences if x == 3])
    print(f"Count of 1's * 3's = {ones * threes}")
