def read_input():
    with open('input.txt') as file:
        return file.read()

def find_trio_with_sum(input, target):
    for x in input:
        for y in input:
            for z in input:
                if x + y + z == target:
                    return x, y, z


if __name__ == '__main__':
    target = 2020       
    input = [int(x) for x in read_input().split()]

    x, y, z = find_trio_with_sum(input, target)
    print(f"{x} + {y} + {z} = {x + y + z}")
    print(f"{x} * {y} * {z} = {x * y * z}")