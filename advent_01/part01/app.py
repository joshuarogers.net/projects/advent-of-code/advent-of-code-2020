def read_input():
    with open('input.txt') as file:
        return file.read()

target = 2020
        
input = [int(x) for x in read_input().split()]
inverse_list = [target - x for x in input]
(x, y) = [x for x in input if x in inverse_list]

print(f"{x} + {y} = {x + y}.")
print(f"{x} * {y} = {x * y}.")