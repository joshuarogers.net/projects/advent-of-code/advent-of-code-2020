import re

def read_input():
    with open('input.txt') as file:
        return file.read()

def parse_passport(text):
    field_sets = text.replace('\n', ' ').split(' ')
    return {x: y for x, y in [x.split(':') for x in field_sets]}

def is_valid_passport(passport):
    def validate_height(x):
        match = re.match("([\d]+)(..)", x)
        groups = match.groups() if match else None
        if not groups:
            return False
        
        height = int(groups[0])
        unit = groups[1].lower()
        
        if unit == 'cm' and 150 <= height <= 193:
            return True
        if unit == 'in' and 59 <= height <= 76:
            return True
        
        return False
    
    validations = {
        'byr': lambda x: 1920 <= int(x) <= 2002,
        'iyr': lambda x: 2010 <= int(x) <= 2020,
        'eyr': lambda x: 2020 <= int(x) <= 2030,
        'hgt': lambda x: validate_height(x),
        'hcl': lambda x: re.match('^#[0-9A-F]{6}$', x.upper()),
        'ecl': lambda x: x in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'],
        'pid': lambda x: len(x) == 9 and x.isnumeric()
    }
    
    return all([x in passport and validations[x](passport[x]) for x in validations])

if __name__ == '__main__':
    raw_lines = read_input().split('\n\n')
    passports = [parse_passport(x) for x in raw_lines]
    valid_passports = [x for x in passports if is_valid_passport(x)]
    print(valid_passports[0])
    print(f"{len(valid_passports)} of {len(raw_lines)} were valid")
