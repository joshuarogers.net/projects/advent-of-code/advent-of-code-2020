def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

if __name__ == '__main__':
    TARGET_SUM = 29221323
    
    numbers = [int(x) for x in read_input()]
    low_index = 0
    high_index = 0
    acc = 0
    
    while high_index < len(numbers):
        if acc < TARGET_SUM:
            acc += numbers[high_index]
            high_index += 1
        elif acc > TARGET_SUM:
            acc -= numbers[low_index]
            low_index += 1
            
        if acc == TARGET_SUM:
            break
    
    addend_range = numbers[low_index:high_index]
    
    print(f"The range between {addend_range[0]} and {addend_range[-1]} sum to {sum(addend_range)}. Answer: {min(addend_range) + max(addend_range)}")
