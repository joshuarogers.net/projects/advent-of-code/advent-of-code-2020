def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

class Processor:
    def __init__(self, opcodes):        
        self.opcodes = opcodes
        self.acc = 0
        self.program_counter = 0
        
    def tick(self):
        asm, arg = self.opcodes[self.program_counter]

        if asm == 'acc':
            self.acc += arg
            
        offset = arg if asm == 'jmp' else 1
        self.program_counter += offset

if __name__ == '__main__':
    opcodes = [(asm, int(arg)) for asm, arg in [x.split(' ') for x in read_input()]]
    processor = Processor(opcodes)
    
    processed_addresses = set([])
    while processor.program_counter not in processed_addresses:
        processed_addresses.add(processor.program_counter)
        processor.tick()
    print(f"Acc: {processor.acc}, PC: {processor.program_counter}")
