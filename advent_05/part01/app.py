def read_input():
    with open('input.txt') as file:
        return file.read().split()

def parse_boarding_pass_index(source):
    return sum([2 ** index for index, x in enumerate(source[::-1]) if x in ['B', 'R']])

def get_seat_id(boarding_pass):
    return (parse_boarding_pass_index(boarding_pass[0:7]) * 8) + parse_boarding_pass_index(boarding_pass[7:])

if __name__ == '__main__':
    passes = [get_seat_id(x) for x in read_input()]
    print(f"The highest boarding pass is: {max(passes)}")
