def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

class BaggageContainer:
    def __init__(self, color, contents):
        self.color = color
        self.contents = contents
        
class BaggageContents:
    def __init__(self, count, color):
        self.count = count
        self.color = color
        
class Graph:
    def __init__(self):
        self.nodes = {}
        
    def add_edge(self, source_id, target_id, *, weight):
        def create_edge_if_not_exists(id):
            if id not in self.nodes:
                self.nodes[id] = []
        
        create_edge_if_not_exists(source_id)
        create_edge_if_not_exists(target_id)
        self.nodes[source_id].append({"id": target_id, "weight": weight})

def count_contained_bags(graph, id):
    def count_contained_bags_recur(id):
        return 1 + sum([count_contained_bags_recur(x["id"]) * x["weight"] for x in graph.nodes[id]])
    return count_contained_bags_recur(id) - 1
        
def parse_rule(x):
    def parse_baggage_content(description):
        count, *parts, _ = description.split()
        return BaggageContents(int(count), ' '.join(parts))

    container, contents = x.split(' bags contain ')
    has_contents = 'no other' not in contents 
    baggage_contents = [parse_baggage_content(x) for x in contents.split(',')] if has_contents else []
    return BaggageContainer(container, baggage_contents)

if __name__ == '__main__':
    graph = Graph()
    baggage_container_rules = [parse_rule(x) for x in read_input()]
    
    for baggage_container_rule in baggage_container_rules:
        for baggage_content in baggage_container_rule.contents:
            graph.add_edge(baggage_container_rule.color, baggage_content.color, weight = baggage_content.count)
        
    print(count_contained_bags(graph, 'shiny gold'))
