def read_input():
    with open('input.txt') as file:
        return file.read().strip().split('\n')

class BaggageContainer:
    def __init__(self, color, contents):
        self.color = color
        self.contents = contents
        
class BaggageContents:
    def __init__(self, count, color):
        self.count = count
        self.color = color
        
class Graph:
    def __init__(self):
        self.nodes = {}
        
    def add_edge(self, source_id, target_id):
        def create_edge_if_not_exists(id):
            if id not in self.nodes:
                self.nodes[id] = set([])
        
        create_edge_if_not_exists(source_id)
        create_edge_if_not_exists(target_id)
        self.nodes[source_id].add(target_id)

def get_children_recursively(graph, id):
    queue = set([id])
    children = set()
    
    while len(queue) > 0:
        current = queue.pop()
        if current not in children:
            children.add(current)
            
        queue |= set(graph.nodes[current])
    return children - set([id])
        
def parse_rule(x):
    def parse_baggage_content(description):
        count, *parts, _ = description.split()
        return BaggageContents(int(count), ' '.join(parts))

    container, contents = x.split(' bags contain ')
    has_contents = 'no other' not in contents 
    baggage_contents = [parse_baggage_content(x) for x in contents.split(',')] if has_contents else []
    return BaggageContainer(container, baggage_contents)

if __name__ == '__main__':
    graph = Graph()
    baggage_container_rules = [parse_rule(x) for x in read_input()]
    
    for baggage_container_rule in baggage_container_rules:
        for baggage_content in baggage_container_rule.contents:
            graph.add_edge(baggage_content.color, baggage_container_rule.color)
        
    print(len(get_children_recursively(graph, 'shiny gold')))
